const ipfsAPI = require('ipfs-api');
// const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });
var ipfs = ipfsAPI({host: 'ec2-18-236-156-22.us-west-2.compute.amazonaws.com', port: 5001, protocol: 'http'});

const MAX_REQUESTS = 50;
const REQ_INTERVAL = 1; // ms

window.storage = {
	start: function() {},

	addFile: async function(content, onSuccess) {
		let buff = new Buffer(content);
		let multiHash;

		ipfs.files.add(buff, { pin: false }).then(function(files) {
			multiHash = files[0].hash;
			if (onSuccess != undefined)
				onSuccess(content, multiHash);
		});
	},

	addFilePromise: function(content) {
		let buff = new Buffer(content);
		return ipfs.files.add(buff, { pin: false }).then(function(files) {
			return files[0].hash; // returns content multhash
		});
	},

	getFile: function(multiHash) {
		return ipfs.files.cat(multiHash);
	},

	getChunks: function(multiHashPipe, callback, clbk_param) {
		let mHashes = multiHashPipe.splice(-MAX_REQUESTS, MAX_REQUESTS);
		let promiseFile = function(multiHash) {
			return new Promise(function(resolve) {
				resolve(storage.getFile(multiHash));
			})
		}

		Promise.all(mHashes.map(promiseFile))
		.then(files => {
			if (typeof(callback) == 'function') {
				console.log(clbk_param)
				callback(clbk_param, files);
			}
			if (multiHashPipe.length > 0) {
				setTimeout(function() {
					storage.getChunks(multiHashPipe, callback);
				}, REQ_INTERVAL);
			}
		});
	}
}


window.addEventListener('load', function() {
	storage.start();
})
