let Queue = require('promise-queue');


const productsAddr = 'GBJCYYE4CYT2Y4KD67XYD75VGXROWPP6VE4CZPWE2L3ZEJNSMVKAEJUL';
const wasteAddr = 'GDEGTECL7ODSTRYS5ULLLPZECC3C6FOIC5MV2COCXERXR3AWGVGJOBH6';
// priv SDMDEFQ4HWQ7KSEEZYKCLVB5ZZ2YBOU7IFOPBK4DQHTVXUXANT7HGK4E
// waste SBAROHAPYJ3XHGDRZJ3SSCZTQ2XNMQQT7KOFY4HXL2AVDT4LVXPLUC4M

const ingredient = 'Ingrediente';
const additives = 'Spezie/Aromi/Additivi';
const vegetables = 'Verdure/Ortaggi/Creme';
const others = 'Salumi/Formaggi/Altro';
const packaging = 'Art. Confezionamento';


window.lot = {
	safe: true,

	products: [],
	productsQueue: null,

	consumedProducts: [],
	consumedProductsQueue: null,

	actionQueue: null,

	start: function() {
		this.actionQueue = new Queue(1);
		this.productsQueue = new Queue();
		this.consumedProductsQueue = new Queue();
		this.getTransactions();
	},

	getAvailableProducts: function(txs) {
		/* TO DO: eveluate efficiency */
		// _.difference(products, _.intersection());
		let consumedHashes = lot.consumedProducts.map(tx => { return tx.memo; });
		return txs.filter(tx => consumedHashes.indexOf(tx.memo) == -1);
	},

	moveToIngredient: function(row) {
		let table = document.getElementById('componentsTable');
		let newRow = table.insertRow(-1);
		newRow.innerHTML = row.innerHTML;
		newRow.deleteCell(2); // Remoiving the 'add' button

		let input = document.createElement('input');
		input.type = 'number';
		input.style = 'width: 100%;';
		newRow.cells[row.cells.length - 2].innerHTML = '';
		newRow.cells[row.cells.length - 2].appendChild(input);

		let cell = newRow.insertCell(-1);
		input = document.createElement('input');
		input.type = 'button';
		input.style = 'width: 100%;';
		input.value = '-';
		input.onclick = function() {
			let table = document.getElementById('componentsTable');
			row.hidden = false;
			table.deleteRow(newRow.rowIndex);
		}
		cell.appendChild(input);
	},

	moveAux: function(originRow, product, tx) {
		let table = document.getElementById('componentsTable');
		let row = table.insertRow(-1);

		let cell = row.insertCell(-1);
		cell.hidden = true;
		cell.innerHTML = tx.memo;

		cell = row.insertCell(-1);
		cell.hidden = true;
		cell.innerHTML = tx.id;

		cell = row.insertCell(-1);
		cell.innerHTML = product.item;
		cell.align = 'center';

		cell = row.insertCell(-1);
		cell.innerHTML = product.product;

		cell = row.insertCell(-1);
		cell.innerHTML = product.lot;
		cell.align = 'center';

		cell = row.insertCell(-1);
		let input = document.createElement('input');
		input.type = 'number';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'button';
		input.style = 'width: 100%;';
		input.value = '-';
		input.onclick = function() {
			let table = document.getElementById('componentsTable');
			originRow.hidden = false;
			table.deleteRow(row.rowIndex);
		}
		cell.appendChild(input);

		console.log(row);
	},

	killProduct: function(rowIndex) {
		let table = document.getElementById('availableComponentsTable');
		let multiHash = table.rows[rowIndex].cells[0].innerHTML;
		let hash = utils.fromBase64toHex(multiHash);

		table.deleteRow(rowIndex);
		this.actionQueue.add(function() {
			this.safe = false;
			let x = document.getElementById("snackbar");
			x.className = "show";
			return ledger.submitMemoHashTx(hash, wasteAddr)
		}).then(() => { lot.checkQueue(lot.actionQueue); });
	},

	killAuxProduct: function(rowIndex, table) {
		let multiHash = table.rows[rowIndex].cells[0].innerHTML;
		let hash = utils.fromBase64toHex(multiHash);

		table.deleteRow(rowIndex);
		this.actionQueue.add(function() {
			this.safe = false;
			let x = document.getElementById("snackbar");
			x.className = "show";
			return ledger.submitMemoHashTx(hash, wasteAddr)
		}).then(() => { lot.checkQueue(lot.actionQueue); });
	},

	displayProducts: function(txs, files) {
		files.forEach(function(file, index) {
			let product = JSON.parse(file);
			if (product.type === ingredient
				|| product.type === undefined)
			{
				let table = document.getElementById('availableComponentsTable');
				let row = table.insertRow(-1);

				let hiddenCell = row.insertCell(-1);
				console.log(txs[index].memo);
				hiddenCell.innerHTML = txs[index].memo;
				hiddenCell.hidden = true;

				hiddenCell = row.insertCell(-1);
				// hiddenCell.innerHTML = multiHashes[index];
				hiddenCell.innerHTML = txs[index].id;
				hiddenCell.hidden = true;

				let input = document.createElement('input');
				input.type = 'button';
				input.value = '+';
				input.style = 'width: 100%;';
				input.onclick = function() {
					lot.moveToIngredient(row);
					row.hidden = true;
				}
				row.insertCell(-1).appendChild(input);

				let cell = row.insertCell(-1);
				cell.innerHTML = product.item;
				cell.align = 'center';
				row.insertCell(-1).innerHTML = product.product;

				cell = row.insertCell(-1);
				cell.innerHTML = product.lot;
				cell.align = 'center';
				// row.insertCell(-1).innerHTML = product.quantity;

				input = document.createElement('input');
				input.type = 'button';
				input.value = 'Scarta';
				input.style = 'width: 100%;';
				input.onclick = function() {
					lot.killProduct(row.rowIndex);
				};

				row.insertCell(-1).appendChild(input);
			}
			else {
				if (product.type == additives) {
					let table = document.getElementById('additives');
					let row = table.insertRow(-1);

					let cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = txs[index].memo;

					cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = file;

					cell = row.insertCell(-1);
					let input = document.createElement('input');
					input.type = 'button';
					input.value = '+';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.moveAux(row, product, txs[index]);
						row.hidden = true;
					}
					cell.appendChild(input);
					let small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.product;
					small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.lot;

					input = document.createElement('input');
					input.type = 'button';
					input.value = '-';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.killAuxProduct(row.rowIndex, table);
					};
					row.insertCell(-1).appendChild(input);
				}
				else if (product.type == vegetables) {
					let table = document.getElementById('vegetables');
					let row = table.insertRow(-1);

					let cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = txs[index].memo;

					cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = file;

					cell = row.insertCell(-1);
					let input = document.createElement('input');
					input.type = 'button';
					input.value = '+';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.moveAux(row, product, txs[index]);
						row.hidden = true;
					}
					cell.appendChild(input);
					let small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.product;
					small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.lot;

					input = document.createElement('input');
					input.type = 'button';
					input.value = '-';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.killAuxProduct(row.rowIndex, table);
					};
					row.insertCell(-1).appendChild(input);
				}
				else if (product.type == others) {
					let table = document.getElementById('others');
					let row = table.insertRow(-1);

					let cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = txs[index].memo;

					cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = file;

					cell = row.insertCell(-1);
					let input = document.createElement('input');
					input.type = 'button';
					input.value = '+';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.moveAux(row, product, txs[index]);
						row.hidden = true;
					}
					cell.appendChild(input);
					let small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.product;
					small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.lot;

					input = document.createElement('input');
					input.type = 'button';
					input.value = '-';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.killAuxProduct(row.rowIndex, table);
					};
					row.insertCell(-1).appendChild(input);
				}
				else if (product.type == packaging) {
					let table = document.getElementById('packaging');
					let row = table.insertRow(-1);

					let cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = txs[index].memo;

					cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = file;

					cell = row.insertCell(-1);
					let input = document.createElement('input');
					input.type = 'button';
					input.value = '+';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.moveAux(row, product, txs[index]);
						row.hidden = true;
					}
					cell.appendChild(input);
					let small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.product;
					small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.lot;

					input = document.createElement('input');
					input.type = 'button';
					input.value = '-';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.killAuxProduct(row.rowIndex, table);
					};
					row.insertCell(-1).appendChild(input);
				}
				else {
					console.log('alt area');
				}
			}
		});
	},

	transformMemos: function(txs) {
		let getHex = function(tx) {
			return new Promise(function(resolve) {
				resolve(utils.fromBase64toHex(tx.memo));
			})
		}

		let getMultiHash = function(hexStr) {
			return new Promise(function(resolve) {
				resolve(utils.fromHexToBase58(hexStr));
			});
		}

		Promise.all(txs.map(getHex))
		.then(hexStrs => Promise.all(hexStrs.map(getMultiHash)))
		.then(multiHashes => storage.getChunks(multiHashes,
			lot.displayProducts,
			txs));
	},

	onTxRecords: function(array, promiseQueue, prevPage, criteria) {
		if (prevPage.records.length > 0) {
			promiseQueue.add(function() { return prevPage.next(); })
			.then(page => lot.onTxRecords(array, promiseQueue, page, criteria));

			let txs = prevPage.records;
			if (Array.isArray(txs)) {
				if (typeof(criteria[0]) == 'function') {
					for (let i = 0; i < criteria.length; i++) {
						txs = criteria[i](txs);
					}
				}
			}
		}
	},

	getTransactions: function() {
		let self = this;

		ledger.getTxs(wasteAddr)
		.then(page => {
			// on waste txs select only the ones incoming from main account
			self.consumedProducts.push(...ledger.filterOutputTxs(page.records));
			console.log(ledger.filterOutputTxs(page.records));
			return page.next();
		})
		.then(page => {
			self.consumedProducts.push(...ledger.filterOutputTxs(page.records));
			return page.next();
		})
		.then(page => {
			if (page.records.length > 0) {
				self.onTxRecords(self.consumedProducts, self.consumedProductsQueue,
					page, [ledger.filterOutputTxs, ledger.filterDataTxs]);
			}

			ledger.getTxs('GAAJGBMOYYO7CCJDYR7O53ZLDTLEJBOSYEB4EMSZCSBBB45KLGL73ORQ')
			.then(page => {
				if (page.records.length > 0) {
					self.onTxRecords(self.products, self.productsQueue, page,
						[ledger.filterInputTxs,
						ledger.filterDataTxs,
						self.getAvailableProducts,
						self.transformMemos]);
				}
			});
		});
	},

	addProduct: function() {
		let table = document.getElementById('productsTable');
		let row = table.insertRow(-1);

		let cell = row.insertCell(-1);
		let input = document.createElement('input');
		input.type = 'text';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'text';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'number';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'button';
		input.style = 'width: 100%;';
		input.value = 'Elimina';
		input.onclick = function() {
			table.deleteRow(row.rowIndex);
		}
		cell.appendChild(input);
	},

	submitLot: function() {
		let productsTable = document.getElementById('productsTable');
		let componentsTable = document.getElementById('componentsTable');

		let productsHeader = Array.from(productsTable.rows[0].cells)
		.map(cell => { return cell.innerHTML; });
		productsHeader.splice(productsHeader.length - 1, 1);
		const products = Array.from(productsTable.rows).slice(1);

		let componentsHeader = Array.from(componentsTable.rows[0].cells)
		.map(cell => { return cell.innerHTML; });
		componentsHeader.splice(componentsHeader.length - 1, 1);
		const components = Array.from(componentsTable.rows).slice(1);

		if (products.length > 0) {
			let lot = {};
			lot['count'] = document.getElementById('idDoc').value;
			lot['lot'] = document.getElementById('idLot').value;
			lot['beaten'] = document.getElementById('radioAbb').checked;

			let files = products.map(product => {
				let file = {};
				// productsHeader.forEach(function(key, index) {
				// 	file[key] = product.cells[index].children[0].value;
				// })
				file['productLot'] = lot.lot;
				file['productCode'] = product.cells[0].children[0].value;
				file['product'] = product.cells[1].children[0].value;
				file['quantity'] = product.cells[2].children[0].value;

				if (components.length > 0) {
					file['components'] = components.map(component => {
						// let componentObj = {};
						// componentObj['componentTxid'] = component.cells[0].innerHTML;

						// componentsHeader.forEach(function(key, index) {
						// 	if (component.cells[index].children[0]) {
						// 		componentObj[key] = component.cells[index].children[0].value;
						// 	} else {
						// 		componentObj[key] = component.cells[index].innerHTML;
						// 	}
						// });
						// return componentObj;
						return component.cells[0].innerHTML;
					})
				}
				return file;
			});

			lot['products'] = files;

			let docRows = document.querySelectorAll('tr');
			Array.from(docRows).map(row => row.hidden = false);

			while (componentsTable.rows.length > 1) {
				componentsTable.deleteRow(-1);
			}
			while (productsTable.rows.length > 1) {
				productsTable.deleteRow(-1);
			}

			storage.addFile(JSON.stringify(lot), this.onLotUpload);
		}
	},

	onLotUpload: function(content, lotMultiHash) {
		let lotInstance = JSON.parse(content);

		let components = lotInstance.products.map(component => {
			component['lotMultiHash'] = lotMultiHash;
			return JSON.stringify(component);
		});

		let getMultiHash = (component) => {
			return new Promise(function(resolve) {
				resolve(storage.addFilePromise(component));
			});
		};

		let getHash = (multiHash) => {
			return new Promise(function(resolve) {
				resolve(utils.fromBase58toHex(multiHash));
			});
		};

		Promise.all(components.map(getMultiHash))
		.then(multiHashes => Promise.all(multiHashes.map(getHash)))
		.then(hashes => {
			hashes.map(hash => {
				lot.actionQueue.add(function() {
					lot.safe = false;

					let x = document.getElementById("snackbar");
					x.className = "show";

					return ledger.submitMemoHashTx(hash, productsAddr);
				})
				.then(() => { lot.checkQueue(lot.actionQueue); });
			});
		});
	},

	checkQueue: function(promiQueue) {
		if (promiQueue.getQueueLength() === 0
			&& promiQueue.getPendingLength() === 0)
		{
			this.safe = true;
			let x = document.getElementById("snackbar");
			x.className = x.className.replace("show", "");
		}
	}
}

window.addEventListener('load', function() {
	lot.start();
});

// onConsumedTx: function(chunk) {
// 	lot.consumedProducts.push(chunk);
// },
//
// onFreshTx: function(chunk) {
// 	lot.products.push(chunk);
// },
