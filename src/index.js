import "./style.css";

if (process.env.NODE_ENV !== 'production') {
	console.log('DEVELOPMENT MODE');
}

const IPFS = require('ipfs-api');
const ipfs = new IPFS({ host: 'ipfs.infura.io',
						port: 5001,
						protocol: 'https' });

import bs58 from 'bs58';

let request = require('request');
let crypto = require('crypto');
let StellarSdk = require('stellar-sdk');

let horizonTestnet = 'https://horizon-testnet.stellar.org';
let horizonTestnetFriendbot = 'https://horizon-testnet.stellar.org/friendbot';
let server = new StellarSdk.Server(horizonTestnet);

let invoiceIssuerPub = 'GDKKLOMGRKNM7LPA57VQB2YVIYAMWMTKYXROSEUN2SJ4ZRRIQJQM6WQE';
let invoiceIssuerPriv = 'SA2B2CJAKHPH42ADUAVTS2WPXU7QCUDBST6QPY7LZ7ODWEOILCKSK3JZ';
let supply = 'GAGNQJOK6KZDXAIUXLJD6D52HNSTAL5PQVZ5R24ZO44H3C2VZREVO457';

const MIN = '0.0000001';

let supplyHistory = [];

let es = server.transactions().forAccount(invoiceIssuerPub)
.cursor('now')
.stream({
		onmessage: function (message) {
			console.log(message);
	}
})

window.supplier = {
	start: function() {
		StellarSdk.Network.useTestNetwork();

		this.prepareInvoice();
		// this.getInvoiceHash();

		return;
	},

	getHash: function(multiHash) {
		return bs58.decode(multiHash).slice(2).toString('hex');
	},

	getMultiHash: function() {
		return;
	},

	getInvoiceHash: async function() {
		let self = this;
		let content = 'MOFO';
		let buff = new Buffer(content);

		let fileHash;

		await ipfs.files.add(buff, { pin: false }).then(function(files) {
			ipfs.files.cat(files[0].hash, function (err, file) {
				console.log(file.toString());
			});

			fileHash = files[0].hash;
			fileHash = self.getHash(fileHash);

			return;
		});

		return fileHash;
	},

	prepareInvoice: async function(contract, ...args) {
		let fileHash = await this.getInvoiceHash();
		console.log(fileHash);

		let issuerPair = StellarSdk.Keypair.fromSecret(invoiceIssuerPriv);

		server.loadAccount(invoiceIssuerPub).then(function(account) {
			let transaction = new StellarSdk.TransactionBuilder(account)
			// Add a payment operation to the transaction
			.addOperation(StellarSdk.Operation.payment({
				destination: supply,
				// The term native asset refers to lumens
				asset: StellarSdk.Asset.native(),
				amount: MIN,
			}))
			.addMemo(StellarSdk.Memo.hash(fileHash))
			.build();

			transaction.sign(issuerPair);

			// Let's see the XDR (encoded in base64) of the transaction we just built
			console.log('xdr: ', transaction.toEnvelope().toXDR('base64'));

			server.submitTransaction(transaction)
			.then(function(transactionResult) {
				console.log(JSON.stringify(transactionResult, null, 2));
				console.log('\nSuccess! View the transaction at: ');
				console.log(transactionResult._links.transaction.href);
			})
			.catch(function(err) {
				console.log('An error has occured:');
				console.log(err);
			});

			return account;
		})
		.catch(function(e) {
			console.error(e);
		});

		return;
	},

	addLot: function() {

		return;
	},

	submitInvoice: async function() {

		return;
	},

	getInvoices: function() {
		server.transactions().forAccount(invoiceIssuerPub).call()
		.then(async function (page) {
			supplyHistory.push(...page.records);

			while (page.records.length > 0) {
				page = await page.next();
				supplyHistory.push(...page.records);
			}

			console.log(supplyHistory);

			return;
		})
		.catch(function (err) {
			console.log(err);
		});

		return;
	}
}

window.addEventListener('load', function() {
	// supplier.start();

	return;
});
